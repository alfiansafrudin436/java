/*
SQLyog Professional v12.4.3 (64 bit)
MySQL - 10.1.36-MariaDB : Database - sistem_hotel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistem_hotel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistem_hotel`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` varchar(10) NOT NULL,
  `nama_admin` varchar(20) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `alamat_admin` varchar(50) DEFAULT NULL,
  `no_hp_admin` varchar(12) DEFAULT NULL,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id`,`nama_admin`,`jenis_kelamin`,`alamat_admin`,`no_hp_admin`,`username`,`password`) values 
('001','Gevi K','Laki-Laki','Cilacap','098765','false','false'),
('002','Joko','Laki-Laki','cilacap','082222222222','admin','admin'),
('003','alfian','Perempuan','bantul','312123','admin','admin');

/*Table structure for table `kamar` */

DROP TABLE IF EXISTS `kamar`;

CREATE TABLE `kamar` (
  `id_kamar` varchar(10) NOT NULL,
  `kelas` varchar(10) DEFAULT NULL,
  `harga` int(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_kamar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kamar` */

insert  into `kamar`(`id_kamar`,`kelas`,`harga`,`status`) values 
('001','SVIP',1008203,'Tersedia'),
('1101','SSVIP',1500000,'Dipakai'),
('1102','SSVIP',1500000,'Dipakai'),
('2101','SVIP',1100000,'Dipakai'),
('2201','SVIP',1100000,'Dipakai'),
('3101','VIP',1000000,'Dipakai'),
('4201','Normal',800000,'Dipakai');

/*Table structure for table `tamu` */

DROP TABLE IF EXISTS `tamu`;

CREATE TABLE `tamu` (
  `id_tamu` varchar(16) NOT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `alamat` varchar(20) DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `tanggal_masuk` date DEFAULT NULL,
  `id_kamar` varchar(10) DEFAULT NULL,
  `id_admin` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_tamu`),
  KEY `id_admin` (`id_admin`),
  KEY `id_kamar` (`id_kamar`),
  CONSTRAINT `tamu_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tamu_ibfk_2` FOREIGN KEY (`id_kamar`) REFERENCES `kamar` (`id_kamar`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tamu` */

insert  into `tamu`(`id_tamu`,`nama`,`jenis_kelamin`,`alamat`,`no_telp`,`tanggal_masuk`,`id_kamar`,`id_admin`) values 
('00123','wqe','Laki-Laki','qwe','eqw','2018-12-12','2201','001'),
('01273812312','Joko','Laki-Laki','bantul','07923123123','2018-12-04','4201','001'),
('081023013','Shanti','Perempuan','Jogja','31231231','2018-06-04','1102','003');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
