/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package systemunpredictable;

import com.mysql.jdbc.Statement;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.util.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;
import static systemunpredictable.Connect.koneksi;

/**
 *
 * @author ACER
 */
public class check_in extends javax.swing.JFrame {

    /**
     * Creates new form check_out
     */
    public check_in() {
        initComponents();
        //MenuUtama callMU= new MenuUtama();
       // callMU.auto_cb();
        auto_cb();
        auto_cb_idadmin();
        this.setLocationRelativeTo(null);
    }

    
    public void auto_cb(){
        try {
            Statement state=(Statement)koneksi().createStatement();
            ResultSet res=state.executeQuery("select id_kamar from kamar WHERE status like '%Tersedia%'");
            String[] colom={"ID"};
            while (res.next()) {
            String id=res.getString("id_kamar");
            cmb_jk_id_kamar.addItem(id);
                
            }
            
            
        } catch (Exception e) {
        }
    }
    
    //public void tanggal(){
        Date add =  Calendar.getInstance().getTime();  
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
        String tanggal=format.format(add);
        
    
    public void auto_cb_idadmin(){
        try {
            Statement state=(Statement)koneksi().createStatement();
            ResultSet res=state.executeQuery("select id from admin");
            String[] colom={"ID"};
            while (res.next()) {
            String id=res.getString("id");
            cmb_jk_id_admin.addItem(id);
                
            }
            
            
        } catch (Exception e) {
        }
    }
    
     private String id,nama,jenis_kelamin,alamat,no_telp,id_kamar,id_admin;
     java.util.Date tanggal_masuk;
     
    private void save(){
        id= txt_id_tamu.getText();
        nama = txt_nama_tamu.getText();
        if(cmb_jk_tamu.getSelectedItem().equals("Laki-Laki")){
            jenis_kelamin="Laki-Laki";
        }
        else if(cmb_jk_tamu.getSelectedItem().equals("Perempuan")){
            jenis_kelamin="Perempuan";
        }
        else {
            JOptionPane.showMessageDialog(null, "tidak terdapat pada pilihan jenis kelamin");
        }
        alamat=txt_alamat_tamu.getText();
        no_telp=txt_no_hp_tamu.getText();
        //tanggal_masuk=txt_tanggal_masuk.getText();
        tanggal_masuk=(java.util.Date)this.calender_checkin.getDate();
        String tanggal_msuk=format.format(tanggal_masuk);
        
        id_kamar=cmb_jk_id_kamar.getSelectedItem().toString();
        id_admin=cmb_jk_id_admin.getSelectedItem().toString();

        
        
    try{
        Statement statement =(Statement)koneksi().createStatement();
        statement.executeUpdate("insert into tamu values('"
            +id+"','"
            +nama+"','"
            +jenis_kelamin+"','"
            +alamat+"','"
            +no_telp+"','"
            +tanggal_msuk+"','"
            +id_kamar+"','"
            +id_admin+"');");
        
        
        statement.executeUpdate("update kamar SET status='"+"Dipakai"+"' where id_kamar='"+cmb_jk_id_kamar.getSelectedItem().toString()+"';");

        statement.close();
        JOptionPane.showMessageDialog(null,"Data Telah Disimpan....");
        clean();
        
        }catch (Exception e){
        JOptionPane.showMessageDialog(null,""+e);
        }
    }
    private void clean(){
        txt_id_tamu.setText("");
        txt_nama_tamu.setText("");
        cmb_jk_tamu.setSelectedItem("---jenis kelamin---");
        txt_alamat_tamu.setText("");
        txt_no_hp_tamu.setText("");
        cmb_jk_id_kamar.setSelectedItem("---No Kamar---");
        cmb_jk_id_admin.setSelectedItem("---Id Admin---");
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_nama_tamu = new javax.swing.JTextField();
        txt_alamat_tamu = new javax.swing.JTextField();
        txt_no_hp_tamu = new javax.swing.JTextField();
        cmb_jk_tamu = new javax.swing.JComboBox<>();
        btn_save_admin = new javax.swing.JButton();
        txt_id_tamu = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cmb_jk_id_kamar = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        cmb_jk_id_admin = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        calender_checkin = new com.toedter.calendar.JDateChooser();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Nama");

        jLabel3.setBackground(new java.awt.Color(0, 0, 0));
        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Jenis Kelamin");

        jLabel4.setBackground(new java.awt.Color(0, 0, 0));
        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Alamat");

        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("No Hp");

        jLabel6.setBackground(new java.awt.Color(0, 0, 0));
        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("No Kamar");

        cmb_jk_tamu.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "---jenis kelamin---", "Laki-Laki", "Perempuan" }));

        btn_save_admin.setBackground(new java.awt.Color(204, 204, 204));
        btn_save_admin.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_save_admin.setText("SIMPAN");
        btn_save_admin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_save_adminActionPerformed(evt);
            }
        });

        jLabel8.setBackground(new java.awt.Color(0, 0, 0));
        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Id/No KTP");

        cmb_jk_id_kamar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "---No Kamar---" }));
        cmb_jk_id_kamar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_jk_id_kamarActionPerformed(evt);
            }
        });

        jLabel12.setBackground(new java.awt.Color(0, 0, 0));
        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("Tanggal Masuk");

        cmb_jk_id_admin.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "---Id Admin---" }));
        cmb_jk_id_admin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_jk_id_adminActionPerformed(evt);
            }
        });

        jLabel11.setBackground(new java.awt.Color(0, 0, 0));
        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText("Id Admin");

        calender_checkin.setDateFormatString("yyyy-MM-dd");
        calender_checkin.setMaxSelectableDate(new java.util.Date(253370743296000L));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel12)
                    .addComponent(jLabel11))
                .addGap(64, 64, 64)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(calender_checkin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmb_jk_id_admin, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_id_tamu, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_no_hp_tamu)
                    .addComponent(txt_nama_tamu)
                    .addComponent(cmb_jk_tamu, 0, 122, Short.MAX_VALUE)
                    .addComponent(txt_alamat_tamu, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmb_jk_id_kamar, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_save_admin)
                .addGap(20, 20, 20))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_id_tamu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_nama_tamu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb_jk_tamu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_alamat_tamu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_no_hp_tamu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(calender_checkin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cmb_jk_id_kamar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb_jk_id_admin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(btn_save_admin, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(51, 204, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("CHECK IN");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("-");
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("X");
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel10MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 274, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel9))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)))
                .addGap(392, 392, 392))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_save_adminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_save_adminActionPerformed
        save();
    }//GEN-LAST:event_btn_save_adminActionPerformed

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        setState(ICONIFIED);
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jLabel10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseClicked
        this.setVisible(false);
    }//GEN-LAST:event_jLabel10MouseClicked

    private void cmb_jk_id_kamarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_jk_id_kamarActionPerformed

    }//GEN-LAST:event_cmb_jk_id_kamarActionPerformed

    private void cmb_jk_id_adminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_jk_id_adminActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_jk_id_adminActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(check_in.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(check_in.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(check_in.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(check_in.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new check_in().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_save_admin;
    private com.toedter.calendar.JDateChooser calender_checkin;
    public javax.swing.JComboBox<String> cmb_jk_id_admin;
    public javax.swing.JComboBox<String> cmb_jk_id_kamar;
    private javax.swing.JComboBox<String> cmb_jk_tamu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txt_alamat_tamu;
    private javax.swing.JTextField txt_id_tamu;
    private javax.swing.JTextField txt_nama_tamu;
    private javax.swing.JTextField txt_no_hp_tamu;
    // End of variables declaration//GEN-END:variables
}
