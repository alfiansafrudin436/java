/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package systemunpredictable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author ACER
 */
public class Connect {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Login A= new Login();
        A.setVisible(true);
        
    }
    private static Connection Connect;
    public static java.sql.Connection koneksi() {
        if(Connect==null){
         String serverName="localhost";
         String databaseName="sistem_hotel";
         
         String user="root";
         String password="";
        try{
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connect=(Connection) DriverManager.getConnection
                    ("jdbc:mysql://"+serverName+"/"+databaseName,user,password);
            
        
        }catch(SQLException e){
            
            JOptionPane.showMessageDialog(null,""+e);
            
        }
        
        }
        return Connect;
    
    }
    
}
